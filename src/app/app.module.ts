import { NgModule, NO_ERRORS_SCHEMA, LOCALE_ID } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

/** Menu Lateral */
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
/** Modais */
import { ModalDialogService } from "nativescript-angular/modal-dialog";
/** Diretivas */
import { MenuModule } from "./directives/menu/menu.module";
import { ContactUserModule } from "./directives/contact-user/contact-user.module";
import { SimpleItemModule } from "./directives/simple-item/simple-item.module";
import { MedicineItemModule } from "./directives/medicine-item/medicine-item.module";
import { RecipeItemModule } from "./directives/recipe-item/recipe-item.module";
import { MaskedTextFieldModule } from "nativescript-masked-text-field/angular";
import { TNSImageCacheItModule } from "nativescript-image-cache-it/angular";
import { registerLocaleData } from "@angular/common";
import ptBr from "@angular/common/locales/pt";
registerLocaleData(ptBr);

import * as platform from "tns-core-modules/platform";
declare var GMSServices: any;

if (platform.isIOS) {
  GMSServices.provideAPIKey("AIzaSyAw7kCZwRD0cSdHU9oSa2DnYiNIkQlb3SA");
}

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    NativeScriptUISideDrawerModule,
    MaskedTextFieldModule,
    TNSImageCacheItModule,
    MenuModule,
    SimpleItemModule,
    MedicineItemModule,
    RecipeItemModule,
    ContactUserModule
  ],
  declarations: [AppComponent],
  providers: [ModalDialogService, { provide: LOCALE_ID, useValue: "pt-PT" }],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
