import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Observable } from "tns-core-modules/data/observable";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { NotificationService } from "./index";
import * as bgHttp from "nativescript-background-http";

@Injectable({
  providedIn: "root"
})
export class ClientService extends Observable {
  private allPath: any;
  private version = "3.2.1";
  constructor(private routerExtensions: RouterExtensions) {
    super();
    this.allPath = {
      // api: "http://172.28.20.49/api/v1"
      api: "https://api-drpediu-dev.doc88.com.br/api/v1"
    };
    this.getVersion();
  }

  /**
   * Verifica a versão disponível.
   */
  public getVersion() {
    if (
      !ApplicationSettings.hasKey("currentVersion") ||
      ApplicationSettings.getString("currentVersion") !== this.version
    ) {
      this.loggout();
    }
  }

  /**
   * Formata de forma serializada o objeto(necessário) para comunicar-se com a API.
   */
  public formatData(params) {
    let formated = Object.keys(params)
      .map(key => {
        return key + "=" + params[key];
      })
      .join("&");

    return formated;
  }

  /**
   * Salva o Token de autorização de consumo da API no localstorage.
   */
  public setApiServiceTokenApi(accessToken) {
    return ApplicationSettings.setString("apiServiceToken", accessToken);
  }

  /**
   * Pega o Token de autorização de consumo da API.
   */
  public getApiServiceToken() {
    return ApplicationSettings.getString("apiServiceToken");
  }

  /**
   * Verifica se o usuário está logado.
   */
  public isLogged() {
    return new Promise(resolve => {
      if (ApplicationSettings.hasKey("userProfile")) {
        resolve(ApplicationSettings.getString("userProfile"));
      }
    });
  }

  /**
   * Pega os dados do perfil do usuário logado.
   */
  public getUserProfile() {
    return JSON.parse(ApplicationSettings.getString("userProfile"));
  }

  /**
   * Apagada os dados de login do usuário
   * e o manda para a tela de login.
   */
  public loggout() {
    ApplicationSettings.clear();
    this.routerExtensions.navigate(["/home"], {
      clearHistory: true,
      transition: {
        name: "fade"
      }
    });
    ApplicationSettings.setString("currentVersion", this.version);
  }

  /**
   * Realiza o cadastro do usuário.
   */
  public registerUser(obj) {
    let params_final = this.formatData(obj);

    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + "/patient-cad-create", {
        method: "POST",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken(),
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          "X-Requested-With": "XMLHttpRequest"
        }),
        body: params_final
      })
        .then(r => r.json())
        .then(response => {
          resolve(response);
          this.resolveResponse("/register-image");
        })
        .catch(e => {
          reject(e);
          console.error(e);
        });
    });
  }

  /**
   * Resgata todo os dados cadastrais do usuário.
   */
  public retrieveUser(UserID) {
    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + "/doctor-cad-get/" + UserID, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken()
        })
      })
        .then(r => r.json())
        .then(response => {
          resolve(response.data);
        })
        .catch(e => {
          reject(new Error(e.message));
        });
    });
  }

  /**
   * Resgata todo os dados cadastrais do usuário.
   */
  public retrievePatientUser(patientID) {
    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + "/doctor-info-cad-patient/" + patientID, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken()
        })
      })
        .then(r => r.json())
        .then(response => {
          resolve(response.data);
        })
        .catch(e => {
          reject(new Error(e.message));
        });
    });
  }

  /**
   * Atualiza os dados dos usuário.
   */
  public updateUser(obj) {
    /** Parâmetros obrigatórios */
    obj["user_id"] = this.getUserProfile().id;
    obj["address_id"] = 1; //Default enquanto não tem o retrieve.
    /** Formatação obrigatórios */
    let params_final = this.formatData(obj);

    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + "/doctor-cad-update", {
        method: "POST",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken(),
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          "X-Requested-With": "XMLHttpRequest"
        }),
        body: params_final
      })
        .then(r => r.json())
        .then(response => {
          resolve(response);
        })
        .catch(e => {
          reject(e);
          console.error(e);
        });
    });
  }

  /**
   * Resgata os estados disponíveis.
   */
  public getInfo(route) {
    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + route, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken()
        })
      })
        .then(r => r.json())
        .then(response => {
          ApplicationSettings.setString("allStates", JSON.stringify(response));
          resolve(response);
        })
        .catch(e => {
          reject(new Error(e.message));
        });
    });
  }

  /**
   * Resgata as cidades de acordo com o estado informado.
   */
  public getCitys(state_id) {
    return new Promise((resolve, reject) => {
      fetch(this.allPath.api + "/patient-cad-citys/" + state_id, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken()
        })
      })
        .then(r => r.json())
        .then(response => {
          resolve(response.citys);
        })
        .catch(e => {
          reject(new Error(e.message));
        });
    });
  }

  /**
   * Realiza a criação do comentário.
   */
  public postAll(route, obj, serialized, notifications) {
    return new Promise(resolve => {
      console.dir(route);
      console.dir(obj);
      fetch(`${this.allPath.api}/${route}`, {
        method: "POST",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken(),
          "Content-Type": serialized
            ? "application/x-www-form-urlencoded;charset=UTF-8"
            : "application/json",
          "X-Requested-With": "XMLHttpRequest"
        }),
        body: serialized ? this.formatData(obj) : JSON.stringify(obj)
      })
        .then(r => r.json())
        .then(response => {
          console.dir(response);
          const finalResponse = response.data ? response.data : response;
          this.showNotificationForUser(finalResponse, notifications);
          resolve(finalResponse);
        })
        .catch(e => {
          NotificationService.prototype.showFeedback({
            type: "error",
            message: e.message
          });
        });
    });
  }

  /**
   * Busca todos os registros.
   */
  public getAll(route, obj, notifications) {
    const finalRoute = obj == null ? `${route}` : `${route}/${obj}`;
    return new Promise(resolve => {
      fetch(`${this.allPath.api}/${finalRoute}`, {
        method: "GET",
        headers: new Headers({
          Accept: "application/json",
          Authorization: "Bearer " + this.getApiServiceToken(),
          "Content-Type": "application/x-www-form-urlencoded"
        })
      })
        .then(r => r.json())
        .then(response => {
          const finalResponse = response.data ? response.data : response;
          this.showNotificationForUser(finalResponse, notifications);
          resolve(finalResponse);
        })
        .catch(e => {
          NotificationService.prototype.showFeedback({
            type: "error",
            message: e.message
          });
        });
    });
  }

  /**
   * Busca todos os registros com paginação.
   */
  public getAllPaginate(route, obj, page, notifications, search = false) {
    return new Promise(resolve => {
      const finalRoute = !search
        ? `${this.allPath.api}/${route}/${obj}?page=${page}`
        : `${this.allPath.api}/${route}/${obj}/${search}?page=${page}`;
      console.dir(finalRoute);
      fetch(finalRoute, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken(),
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        })
      })
        .then(r => r.json())
        .then(response => {
          const finalResponse = response.data ? response.data : response;
          console.dir(finalResponse);
          this.showNotificationForUser(finalResponse, notifications);
          resolve(finalResponse);
        })
        .catch(e => {
          NotificationService.prototype.showFeedback({
            type: "error",
            message: e.message
          });
        });
    });
  }

  /**
   * Busca um registro específico pelo ID.
   */
  public getById(route, id) {
    return new Promise((resolve, reject) => {
      fetch(`${this.allPath.api}/${route}/${id}`, {
        method: "GET",
        headers: new Headers({
          Authorization: "Bearer " + this.getApiServiceToken()
        })
      })
        .then(r => r.json())
        .then((response: any) => {
          const finalResponse = response.data ? response.data : response;
          resolve(finalResponse);
        })
        .catch(e => {
          NotificationService.prototype.showFeedback({
            type: "error",
            message: e.message
          });
        });
    });
  }

  /**
   * Exibe as notificações das resquisições ao usuário.
   */
  public showNotificationForUser(finalResponse, notifications) {
    const group = finalResponse.errors ? "error" : "success";
    if (
      Array.isArray(notifications) &&
      notifications.filter(item => item.toUpperCase() === group.toUpperCase())
        .length
    ) {
      NotificationService.prototype.showFeedback({
        type: group,
        message: finalResponse.success ? finalResponse.success : finalResponse
      });
    }
  }

  /**
   * Pega a resposta e trata ela de 3 maneiras [errors, success, status].
   */
  public resolveResponse(path) {
    this.routerExtensions.navigate([path], {
      clearHistory: true,
      transition: {
        name: "slideLeft"
      }
    });
  }

  /**
   * Realiza upload de arquivo sem conversão.
   */
  public uploadFile(route, obj, fileName, filePath) {
    return new Promise((resolve, reject) => {
      let session = bgHttp.session("image-upload");
      const request = {
        url: `${this.allPath.api}/${route}`,
        method: "POST",
        headers: {
          "Content-Type": "application/octet-stream",
          "X-Requested-With": "XMLHttpRequest",
          Authorization: "Bearer " + this.getApiServiceToken(),
          "File-Name": fileName
        },
        description: ""
      };
      let params = [
        {
          name: "image",
          filename: filePath,
          mimeType: "image/jpeg"
        }
      ];
      let final = this.adjustmentDataToBackgroundFile(obj);

      let task = session.multipartUpload(final.concat(params), request);
      task.on("complete", resolve);
      task.on("error", reject);
    });
  }

  /**
   * Realiza ajuste de dados necessários para o serviço de upload de arquivo.
   */
  public adjustmentDataToBackgroundFile(obj) {
    let list = [];
    Object.keys(obj).forEach(item => {
      list.push({ name: item, value: obj[item].toString() });
    });
    return list;
  }
}
