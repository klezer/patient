import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class GoogleAutoCompleteService {

  /**
   * Realiza a busca do endereço com baseado no GMaps.
   */
  public getAddressByInfo(Address, WithKey) {
    const endPoint = 'https://maps.googleapis.com/maps/api/geocode/json';
    let apiKey = 'AIzaSyAw7kCZwRD0cSdHU9oSa2DnYiNIkQlb3SA';
    let region = 'BR';
    let path = (WithKey) ? `${endPoint}?address=${Address}&sensor=false&region=${region}&key=${apiKey}` : `${endPoint}?address=${Address}&sensor=false&region=${region}`;
    return new Promise((resolve, reject) => {
      fetch(path)
      .then((r) => r.json())
        .then((response) => {
          if(response.status == 'OVER_QUERY_LIMIT'){
            /** Alternativa para o esgotamento da API */
            response = this.getAddressByInfo(Address, false);
          }
          if (response['results'] && 
              response['results'][0] && 
              response['results'][0].address_components) {
              resolve(response['results'][0].address_components);
            }
        })
        .catch((e) => {
          reject(new Error(e.message))
        });
      });
  }

  /**
   * Realiza a filtragem do endereço.
   */
  public filterAddress(address) {
    return {
      street_title: this.getStreet(address), 
      district_title: this.getDistrictTitle(address), 
      mycity: this.getCity(address), 
      state: this.getState(address)
    }
  }

  /**
   * Pega a rua.
   */
  public getStreet(address) {
    return (address[1] && (address[1]["types"][0] == "route")) ? address[1]["long_name"] : null;
  }

  /**
   * Pega o bairro.
   */
  public getDistrictTitle(address) {
    if(address[1] && (address[1]["types"][1] == "sublocality" && address[1]["types"][2] == "sublocality_level_1")){
      return address[1]["long_name"];
    } else if(address[2] && (address[2]["types"][1] == "sublocality" && address[2]["types"][2] == "sublocality_level_1")){
      return address[2]["long_name"];
    }
  }

  /**
   * Pega a cidade.
   */
  public getCity(address) {
    if(address[1] && (address[1]["types"][0] == "administrative_area_level_2")){
      return address[1]["long_name"];
    } else if(address[2] && (address[2]["types"][0] == "administrative_area_level_2")){
      return address[2]["long_name"];
    } else if(address[3] && (address[3]["types"][0] == "administrative_area_level_2")){
      return address[3]["long_name"];
    }
  }

  /**
   * Pega o estado.
   */
  public getState(address) {
    if(address[2] && (address[2]["types"][0] == "administrative_area_level_1")){
      return address[2]["short_name"];
    } else if(address[3] && (address[3]["types"][0] == "administrative_area_level_1")){
      return address[3]["short_name"];
    } else if(address[4] && (address[4]["types"][0] == "administrative_area_level_1")){
      return address[4]["short_name"];
    }
  }

}
