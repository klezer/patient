import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: "~/app/screens/public/home/home.module#HomeModule"
  },
  {
    path: "register",
    loadChildren: "~/app/screens/public/register/register.module#RegisterModule"
  },
  {
    path: "register-image",
    loadChildren:
      "~/app/screens/private/register-image/register-image.module#RegisterImageModule"
  },
  {
    path: "register-image/:user_id",
    loadChildren:
      "~/app/screens/private/register-image/register-image.module#RegisterImageModule"
  },
  {
    path: "terms",
    loadChildren: "~/app/screens/public/terms/terms.module#TermsModule"
  },
  {
    path: "dashboard",
    loadChildren:
      "~/app/screens/private/my-treatments/my-treatments.module#MyTreatmentsModule"
  },
  {
    path: "my-recipes",
    loadChildren:
      "~/app/screens/private/my-recipes/my-recipes.module#MyRecipesModule"
  },
  {
    path: "my-doctors",
    loadChildren:
      "~/app/screens/private/my-doctors/my-doctors.module#MyDoctorsModule"
  },
  {
    path: "my-doctors-details/:other_user_id",
    loadChildren:
      "~/app/screens/private/my-doctors-details/my-doctors-details.module#MyDoctorsDetailsModule"
  },
  {
    path: "my-calendar",
    loadChildren:
      "~/app/screens/private/my-calendar/my-calendar.module#MyCalendarModule"
  },
  {
    path: "drug-disposal",
    loadChildren:
      "~/app/screens/private/drug-disposal/drug-disposal.module#DrugDisposalModule"
  },
  {
    path: "my-profile",
    loadChildren:
      "~/app/screens/private/my-profile/my-profile.module#MyProfileModule"
  },
  {
    path: "anamnesis",
    loadChildren:
      "~/app/screens/private/anamnesis/anamnesis.module#AnamnesisModule"
  },
  {
    path: "recipes-document/:recipe_id",
    loadChildren:
      "~/app/screens/private/recipes-document/recipes-document.module#RecipesDocumentModule"
  },
  {
    path: "messages",
    loadChildren:
      "~/app/screens/private/messages/messages.module#MessagesModule"
  },
  {
    path: "messages-details/:recipe_id/:other_user_id",
    loadChildren:
      "~/app/screens/private/messages-details/messages-details.module#MessagesDetailsModule"
  },
  {
    path: "invite",
    loadChildren: "~/app/screens/private/invite/invite.module#InviteModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
