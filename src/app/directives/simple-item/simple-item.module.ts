import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { SimpleItemComponent } from "./simple-item.component";

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule
    ],
    declarations: [
        SimpleItemComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        SimpleItemComponent
    ]
})
export class SimpleItemModule { }
