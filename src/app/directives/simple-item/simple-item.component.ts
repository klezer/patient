import { Component, Input } from '@angular/core';
import * as moment from 'moment'; 
import 'moment/min/locales';

@Component({
    moduleId: module.id,
    selector: 'simple-item, [simple-item]',
    templateUrl: './simple-item.component.html',
    styleUrls: ['./simple-item.component.scss']
})

export class SimpleItemComponent {
    @Input('item') item: any;
    @Input('image-field') imageField: any;
    @Input('title-field') titleField: any;
    @Input('description-field') descriptionField: any;
    @Input('time-field') timeField?: any;
    @Input('link-field') linkField?: any;
    @Input('badge-field') badgeField?: any;
    @Input('activated-field') activatedField?: boolean;

    constructor() { }

    /**
     * Formata a data exibindo a quanto tempo ocorrreu o comentário.
     */
    public formatDate(date) {
       return moment(moment.unix(parseInt(date)), 'YYYYMMDD').locale('pt-br').fromNow();
    }

}
