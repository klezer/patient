import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { MenuComponent } from "./menu.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";
/** Menu Lateral */
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        NativeScriptUISideDrawerModule,
        TNSFontIconModule.forRoot({
			"icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        MenuComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MenuComponent
    ]
})
export class MenuModule { }
