import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { RecipeItemComponent } from "./recipe-item.component";

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        TNSFontIconModule.forRoot({
            "icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        RecipeItemComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        RecipeItemComponent
    ]
})
export class RecipeItemModule { }
