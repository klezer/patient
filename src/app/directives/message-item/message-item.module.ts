import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MessageItemComponent } from "./message-item.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        TNSFontIconModule.forRoot({
            "icomoon": "./fonts/icon-font-icomoon.css"
        }),
        NativeScriptUIListViewModule
    ],
    declarations: [
        MessageItemComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MessageItemComponent
    ]
})
export class MessageItemModule { }
