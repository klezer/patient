import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { SearchTopBarComponent } from "./search-top-bar.component";

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        TNSFontIconModule.forRoot({
			"icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        SearchTopBarComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        SearchTopBarComponent
    ]
})
export class SearchTopBarModule { }
