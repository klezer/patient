import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as email from "nativescript-email";
import * as TNSPhone from "nativescript-phone";
import { NotificationService } from "../../services/index";

@Component({
  moduleId: module.id,
  selector: "contact-user, [contact-user]",
  templateUrl: "./contact-user.component.html",
  styleUrls: ["./contact-user.component.scss"]
})
export class ContactUserComponent {
  @Input("contact") public userContact: any;
  @Input("show-recipe") public showRecipe: boolean;
  @Input("show-contact") public showContact: boolean;
  @Input("show-info") public showInfo: boolean;
  @Output("show-external") public showExternal = new EventEmitter<any>();

  constructor(public notificationService: NotificationService) {}

  /**
   * Enviar e-mail.
   */
  public sendEmail(to) {
    email.available().then((avail: boolean) => {
      if (avail) {
        email
          .compose({
            subject: "Dr Pediu - Contato Médico",
            body: `Olá <strong>${this.userContact.name}</strong>,`,
            to: [to],
            cc: [],
            bcc: [],
            attachments: []
          })
          .then(
            () => {
              console.log("Email composer closed");
            },
            err => {
              console.log("Error: " + err);
            }
          );
      } else {
        this.notificationService.showFeedback({
          type: "warning",
          message: "E-mail não configurado neste aparelho."
        });
      }
    });
  }

  /**
   * Realizar ligação.
   */
  public doCall(phoneNumber) {
    TNSPhone.requestCallPermission(
      "Você deve aceitar a permissão para poder fazer uma ligação direta."
    )
      .then(() => TNSPhone.dial(phoneNumber, false))
      .catch(() => TNSPhone.dial(phoneNumber, true));
  }
}
