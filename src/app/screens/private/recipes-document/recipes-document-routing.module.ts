import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { RecipesDocumentComponent } from "./recipes-document.component";

const routes: Routes = [
    { path: "", component: RecipesDocumentComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class RecipesDocumentRoutingModule { }
