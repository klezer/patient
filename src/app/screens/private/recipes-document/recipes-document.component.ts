import { Component, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ClientService, UtilsService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import * as application from "tns-core-modules/application";
import { Page } from "tns-core-modules/ui/page";
import { isAndroid } from "tns-core-modules/platform";
import { ModalComponent } from "./../../../directives/modal/modal.component";
import { Switch } from "tns-core-modules/ui/switch";
import { RouterExtensions } from "nativescript-angular/router";
import { ShareFile } from "nativescript-share-file";
import * as fs from "tns-core-modules/file-system";
import { getFile, HttpRequestOptions } from "tns-core-modules/http";
import * as moment from "moment";
@Component({
  selector: "RecipesDocument",
  moduleId: module.id,
  templateUrl: "./recipes-document.component.html",
  styleUrls: ["./recipes-document.component.scss"]
})
export class RecipesDocumentComponent {
  public delayTimer: any;
  public userProfile: any;
  public isAndroid = isAndroid;
  public loadedAsync: boolean = false;
  public recipe: any = [];
  public recipeID: any;
  public routers = {
    downloadPDF:
      "https://api-drpediu-dev.doc88.com.br/api/patient-generate-recipe-pdf",
    getRecipe: "patient-recipe-doctor",
    startTreatment: "patient-treatment-create",
    recipeFileToShare: "patient-generate-recipe-pdf"
  };
  public allActions: any;
  public displayOption = {
    listOptions: true,
    confirmOptions: false,
    showMessage: false
  };
  public selectedMedicines: any = [];
  public allowNotification: boolean = true;
  public canStart: boolean = true;
  public myImagePin: string = `~/image-source/Pin-${
    isAndroid ? "android" : "ios"
  }.png`;
  public fileName: any;
  public documents: any;
  public path: any;
  public file: any;
  public shareFile = new ShareFile();
  public moment = moment;
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private clientService: ClientService,
    private routerExtensions: RouterExtensions,
    private utilsService: UtilsService,
    public page: Page
  ) {
    /** Remove o Action Bar padrão */

    this.page.actionBarHidden = true;
    /** Resgata os dados do usuário logado */
    this.userProfile = JSON.parse(ApplicationSettings.getString("userProfile"));
    /** Recebe os parâmetros da rota */
    this.ActivatedRoute.params.forEach(params => {
      this.recipeID = params.recipe_id;
    });

    /** Ações disponíveis para a receita */
    this.allActions = [
      {
        title: "Enviar",
        icon: "mdi-email",
        background: "#1C5586",
        width: "18%",
        primary: false,
        disabled: false,
        method: "send"
      },
      {
        title: "Cotar",
        icon: "mdi-attach-money",
        background: "#1C5586",
        width: "18%",
        primary: false,
        disabled: true,
        method: "money"
      },
      {
        title: "Comprar",
        icon: "mdi-lock",
        background: "#1C5586",
        width: "18%",
        primary: false,
        disabled: true,
        method: "buy"
      },
      {
        title: "Iniciar Tratamento",
        icon: "mdi-play-arrow",
        background: "#5CADCD",
        width: "28%",
        primary: true,
        disabled: false,
        method: "start"
      }
    ];
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getRecipeInfo();
    }, 450);
  }

  /**
   * Pega as informações da receita.
   */
  public getRecipeInfo() {
    this.clientService
      .getById(this.routers.getRecipe, this.recipeID)
      .then(response => {
        this.recipe = response;
        this.loadedAsync = true;
        this.canStartTreatment();
      });
  }

  /**
   * Exibe os modais de acordo com as ação desejada pelo usuário.
   */
  public resolveMethod(args, action) {
    if (action == "start" && this.canStart) {
      this.modal.show();
    } else if (action == "send") {
      this.shareRecipe();
    }
  }

  /**
   * Seleciona ou não os medicamentos disponíveis.
   */
  public selectOption(item) {
    item.selected = item.selected !== undefined ? !item.selected : true;
    // Verifica a seleção do medicamento
    this.selectedMedicines = this.recipe.medicines.filter(item => {
      return item.selected == true;
    });
  }

  /**
   * Ao selecionar se deseja receber notificações.
   */
  public onChecked(args) {
    this.allowNotification = (<Switch>args.object).checked;
  }

  /**
   * Inicia o tratamento.
   * Prepara o objeto final e submete os dados.
   */
  public startTreatment() {
    let start = {
      user_id: this.userProfile.id,
      medicals: this.selectedMedicines,
      notifications: this.allowNotification
    };
    // Submete o ínicio
    this.clientService
      .postAll(this.routers.startTreatment, start, false, ["error"])
      .then(response => {
        // Fecha o modal e atualiza as informações
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
          this.getRecipeInfo();
          this.closeModal();
          this.utilsService.goToScreen("/dashboard");
        }, 2000);
      });
  }

  /**
   * Verifica se alguma medicação da receita pode ser iniciada.
   */
  public canStartTreatment() {
    let options = this.recipe.medicines.filter(
      item => item.start_medical == true
    );
    this.canStart = !(options.length == this.recipe.medicines.length);
  }

  /**
   * Fecha o modal.
   */
  public closeModal() {
    this.modal.hide();
  }

  /**
   * Escuta a abertura do Modal.
   */
  public onOpenModal() {
    console.log("opened modal");
  }

  /**
   * Escuta o fechamento do Modal.
   * Reseta as variáveis para iniciar o processo.
   */
  public onCloseModal() {
    this.selectedMedicines = [];
    this.changeScreen("selectedMedicines");
    this.recipe.medicines.forEach(item => {
      item.selected = false;
    });
  }

  /**
   * Envia para a tela devida.
   */
  public goToScreen(route) {
    this.routerExtensions.navigate([route], {
      clearHistory: true,
      transition: {
        name: "fade"
      }
    });
  }
  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
    if (screen == "showMessage") {
      this.startTreatment();
    }
  }

  /**
   * Compartilha a receita.
   */
  public shareRecipe() {
    const time = Math.round(+new Date() / 1000);
    const fileName = `${time}r${this.recipeID}.pdf`;
    this.loadedAsync = false;
    if (isAndroid) {
      const path = android.os.Environment.getExternalStoragePublicDirectory(
        android.os.Environment.DIRECTORY_DOWNLOADS
      ).getAbsolutePath();
      const downloadedFilePath = fs.path.join(path, fileName);
      const permissions = require("nativescript-permissions");
      permissions
        .requestPermission(
          android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
          "Eu preciso dessas permissões para acessar o arquivo da receita"
        )
        .then(() => {
          this.getRecipeFile(downloadedFilePath);
        });
    } else {
      const path = fs.knownFolders.ios.library().path;
      const downloadedFilePath = fs.path.join(path, fileName);
      this.getRecipeFile(downloadedFilePath);
    }
  }

  /**
   * Realiza o download do arquivo da receita.
   */
  public getRecipeFile(downloadedFilePath) {
    const header: HttpRequestOptions = {
      url: `${this.routers.downloadPDF}/${this.recipeID}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + this.clientService.getApiServiceToken(),
        "X-Requested-With": "XMLHttpRequest"
      }
    };
    getFile(header, downloadedFilePath).then(
      resultFile => {
        this.loadedAsync = true;
        if (fs.File.exists(downloadedFilePath)) {
          if (isAndroid) {
            this.shareAndroid(resultFile);
          } else {
            this.shareIos(resultFile);
          }
        }
      },
      error => {
        alert({
          title: "Error",
          okButtonText: "OK",
          message: `${error}`
        });
      }
    );
  }

  /**
   * Compartilhamento para o Android.
   */
  public shareAndroid(args) {
    let intent = new android.content.Intent();
    intent.addFlags(android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION);

    let uris = new java.util.ArrayList();
    uris.add(args.path);
    let builder = new android.os.StrictMode.VmPolicy.Builder();
    android.os.StrictMode.setVmPolicy(builder.build());

    intent.setAction(android.content.Intent.ACTION_SEND);
    intent.setType("application/pdf");
    intent.putExtra(
      android.content.Intent.EXTRA_STREAM,
      android.net.Uri.parse("file://" + args.path)
    );

    application.android.context.startActivity(
      android.content.Intent.createChooser(
        intent,
        args.intentTitle ? args.intentTitle : "Compartilhar Receita com:"
      )
    );
  }

  /**
   * Compartilhamento para o Android.
   */
  public shareIos(args) {
    const appPath = this.getCurrentAppPath();
    const path = args.path.replace("~", appPath);
    console.dir(path);
    this.shareFile.open({
      path: path,
      options: true,
      animated: true
    });
  }

  /**
   * Pega pasta de arquivos do iOS.
   */
  private getCurrentAppPath(): string {
    const currentDir = __dirname;
    const tnsModulesIndex = currentDir.indexOf("/tns_modules");

    // Module not hosted in ~/tns_modules when bundled. Use current dir.
    let appPath = currentDir;
    if (tnsModulesIndex !== -1) {
      // Strip part after tns_modules to obtain app root
      appPath = currentDir.substring(0, tnsModulesIndex);
    }
    return appPath;
  }
}
