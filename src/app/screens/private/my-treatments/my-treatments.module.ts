import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyRecipesRoutingModule } from "./my-treatments-routing.module";
import { MyTreatmentsComponent } from "./my-treatments.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { ModalModule } from "./../../../directives/modal/modal.module";
import { MedicineItemModule } from "./../../../directives/medicine-item/medicine-item.module";
@NgModule({
    imports: [
        ModalModule,
        MedicineItemModule,
        NativeScriptCommonModule,
        MyRecipesRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        MyTreatmentsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MyTreatmentsComponent
    ]
})
export class MyTreatmentsModule { }
