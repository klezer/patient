import { Component, ViewChild } from "@angular/core";
import {
  ClientService,
  EventEmitterService,
  NotificationService
} from "./../../../services";
import { isAndroid } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as moment from "moment";
import "moment/min/locales";
import { ModalComponent } from "./../../../directives/modal/modal.component";
import { screen } from "tns-core-modules/platform";
import { isNumber } from "tns-core-modules/utils/types";
import { LocalNotifications } from "nativescript-local-notifications";
import { Color } from "tns-core-modules/color";

@Component({
  selector: "MyTreatments",
  moduleId: module.id,
  templateUrl: "./my-treatments.component.html",
  styleUrls: ["./my-treatments.component.scss"]
})
export class MyTreatmentsComponent {
  public delayTimer: any;
  public isAndroid = isAndroid;
  public recipes: any = [];
  public loadedAsync: boolean = false;
  public userProfile: any;
  public listKeys: any = [];
  public routers = {
    retrieveAll: "patient-slice-recipe",
    updateMedicine: "patient-slice-medical-update",
    retrieveAllRecipes: "patient-list-recipe",
    retrieveSchedule: "patient-treatment-calendar"
  };
  public searchInfo: string;
  public emptyList: boolean = true;
  public currentPage: number = 0;
  public today: any;
  public medicines: any = [];
  public selectedMedicine: any = null;
  public screen = screen;
  public displayOption = {
    neverStarted: false,
    takenToday: false,
    emptyToday: false
  };
  public isNumber = isNumber;
  public hasRecipes: any = [];
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;

  constructor(
    private clientService: ClientService,
    private notificationService: NotificationService,
    public page: Page
  ) {
    this.monitorPushMessages();
    this.page.actionBarHidden = true;
    this.getUserInfo();
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
    this.today = moment()
      .locale("pt-br")
      .format("LL");
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
    this.getAllSchedules();
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAllMedicines();
    }, 450);
  }

  /**
   * Monitora o Push Notification.
   */
  public monitorPushMessages() {
    EventEmitterService.get("event-receita").subscribe({
      next: () => {
        this.getAllMedicines();
      }
    });
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Busca os medicamentos de hoje.
   * */
  public getAllMedicines() {
    this.clientService
      .getAll(this.routers.retrieveAll, this.userProfile.id, ["error"])
      .then((response: any) => {
        this.loadedAsync = true;
        this.medicines = response;
        this.getAllRecipes();
      });
  }

  /**
   * Busca tratamentos disponíveis.
   * */
  public getAllRecipes() {
    this.clientService
      .getAll(this.routers.retrieveAllRecipes, this.userProfile.id, ["error"])
      .then((response: any) => {
        this.hasRecipes = response;
        this.showOptionsView();
      });
  }

  /**
   * Abre/fecha menu
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * Abre o modal.
   */
  public showModal(item) {
    if (item.id !== undefined) {
      this.selectedMedicine =
        item.schedule_generate == 0 ? item : this.resolveDates(item);
      this.modal.show();
    }
  }

  /**
   * Aplica as datas de início e final do tratamento.
   */
  public resolveDates(item) {
    item.start_medical = true;
    item.start_period = moment().format("DD/MM/YYYY");
    item.end_period = moment()
      .add(item.number_of_days, "days")
      .format("DD/MM/YYYY");
    return item;
  }

  /**
   * Fecha o modal.
   */
  public closeModal() {
    this.modal.hide();
  }

  /**
   * Escuta o fechamento do Modal.
   * Reseta as variáveis para iniciar o processo.
   */
  public onCloseModal() {
    this.selectedMedicine = null;
  }

  /**
   * Envia a informação da interação do medicamento.
   */
  public sendInformationAboutMedication(confirm) {
    let info = {
      id: this.selectedMedicine.id,
      taken: confirm
    };
    this.clientService
      .postAll(this.routers.updateMedicine, info, false, ["error"])
      .then(() => {
        this.closeModal();
        this.showResultMessage(confirm);
      });
  }

  /**
   * Exibe a confirmação da interação com o medicamento.
   */
  public showResultMessage(confirm) {
    this.notificationService.showFeedback({
      type: "success",
      message: `Medicamento ${confirm ? "tomado" : "pulado"} com sucesso.`
    });
    this.loadedConfirm();
  }

  /**
   * Exibe as possíveis telas caso não haja nenhum medicamento listado.
   */
  public showOptionsView() {
    let allMedicines = this.medicines.filter(item => isNumber(item.taken));
    let allTreatmentes = this.hasRecipes.filter(item => !item.started_out);
    // Se tem um medicamento novo que não foi iniciado
    if (this.medicines.length == allMedicines.length && allTreatmentes.length) {
      this.changeScreen("neverStarted");
    }
    // Se houve medicamentos para hoje e já foram tomados
    else if (this.medicines.length == allMedicines.length) {
      this.changeScreen("takenToday");
    }
    // Se não houve houve medicamentos para hoje
    else if (!this.medicines.length) {
      this.changeScreen("emptyToday");
    }
  }

  /**
   * Busca os eventos.
   * */
  public getAllSchedules() {
    this.clientService
      .getAll(this.routers.retrieveSchedule, this.userProfile.id, ["error"])
      .then((response: any) => {
        if (response.length > 0) {
          LocalNotifications.cancelAll();
          response.forEach(item => {
            let date = new Date(item.start_period * 1000);
            this.createScheduleNotifications(item, date);
          });
        }
        this.loadedAsync = true;
      });
  }

  /**
   * Define o agendamento das notificações.
   */
  public createScheduleNotifications(item, date) {
    LocalNotifications.schedule([
      {
        id: item.id,
        title: "Está na hora do seu medicamento!",
        body: `Olá, está na hora de tomar o seu ${item.title}, por favor, confirme na sua tela de tratamentos.`,
        bigTextStyle: true,
        color: new Color("#4FB7DB"),
        badge: 1,
        thumbnail: true,
        interval: null,
        channel: "Channel",
        sound: "alarm.wav",
        at: new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          date.getHours(),
          date.getMinutes()
        ),
        forceShowWhenInForeground: true
      }
    ]);
  }

  /**
   * needTakeMedication
   */
  public needTakeMedication() {
    return this.medicines.filter(item => item.schedule_generate == 0).length;
  }

  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }
}
