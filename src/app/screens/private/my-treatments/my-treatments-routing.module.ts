import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyTreatmentsComponent } from "./my-treatments.component";

const routes: Routes = [
    { path: "", component: MyTreatmentsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyRecipesRoutingModule { }
