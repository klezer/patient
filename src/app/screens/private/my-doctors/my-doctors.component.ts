import { Component } from "@angular/core";
import { ClientService } from "./../../../services";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import "moment/min/locales";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Page } from "tns-core-modules/ui/page";
import { screen } from "tns-core-modules/platform";

@Component({
  selector: "MyDoctors",
  moduleId: module.id,
  templateUrl: "./my-doctors.component.html",
  styleUrls: ["./my-doctors.component.scss"]
})
export class MyDoctorsComponent {
  public delayTimer: any;
  public profile: any;
  public isAndroid = isAndroid;
  public isIOS = isIOS;
  public screen = screen;
  public searchQuery: string;
  public myDoctors: any = [];
  public options: any;
  public now: any;
  public loadedAsync: boolean = false;
  public userProfile: any;
  public listKeys: any = [];
  public counter = 0;
  public menuList;
  public routers = {
    retrieveAll: "patient-list-doctor"
  };
  public currentPage = 1;
  public displayOption = {
    emptyList: true,
    hasList: false
  };

  constructor(private clientService: ClientService, public page: Page) {
    this.page.actionBarHidden = true;
    this.getUserInfo();
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Abre/fecha menu
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAll(false);
    }, 450);
  }

  /**
   * Pega os itens.
   */
  public getAll(search) {
    this.clientService
      .getAllPaginate(
        this.routers.retrieveAll,
        this.userProfile.id,
        this.currentPage,
        ["error"],
        search
      )
      .then((response: any) => {
        this.loadedAsync = true;
        if (response.length > 0) {
          this.changeScreen("hasList");
          this.currentPage = this.currentPage + 1;
          response.forEach(item => {
            this.myDoctors.push(item);
          });
        }
      });
  }
  /**
   * Pagina mais itens.
   */
  public loadMore() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAll(false);
    }, 1000);
  }

  /**
   * Realiza a consulta do termo bucado.
   */
  public search(searchInfo) {
    this.loadedAsync = false;
    this.currentPage = 1;
    this.myDoctors = [];
    this.getAll(searchInfo.length > 0 ? searchInfo : false);
  }

  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }

  /** Abre/fecha menu  */
  public closeSibar(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
  }
}
