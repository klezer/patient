import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyDoctorsComponent } from "./my-doctors.component";

const routes: Routes = [
    { path: "", component: MyDoctorsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyDoctorsRoutingModule { }
