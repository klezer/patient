import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyDoctorsRoutingModule } from "./my-doctors-routing.module";
import { MyDoctorsComponent } from "./my-doctors.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { SearchTopBarModule } from './../../../directives/search-top-bar/search-top-bar.module';
import { SimpleItemModule } from './../../../directives/simple-item/simple-item.module';

@NgModule({
    imports: [
        SearchTopBarModule,
        SimpleItemModule,
        NativeScriptCommonModule,
        MyDoctorsRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        MyDoctorsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MyDoctorsComponent
    ]
})
export class MyDoctorsModule { }
