import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyRecipesRoutingModule } from "./my-recipes-routing.module";
import { MyRecipesComponent } from "./my-recipes.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { SearchTopBarModule } from './../../../directives/search-top-bar/search-top-bar.module';
import { RecipeItemModule } from './../../../directives/recipe-item/recipe-item.module';

@NgModule({
    imports: [
        SearchTopBarModule,
        RecipeItemModule,
        NativeScriptCommonModule,
        MyRecipesRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        MyRecipesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MyRecipesComponent
    ]
})
export class MyRecipesModule { }
