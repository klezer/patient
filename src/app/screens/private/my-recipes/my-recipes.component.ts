import { Component } from "@angular/core";
import { ClientService } from "./../../../services";
import { isAndroid } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { screen } from "tns-core-modules/platform";
@Component({
  selector: "MyRecipes",
  moduleId: module.id,
  templateUrl: "./my-recipes.component.html",
  styleUrls: ["./my-recipes.component.scss"]
})
export class MyRecipesComponent {
  public delayTimer: any;
  public isAndroid = isAndroid;
  public recipes: any = [];
  public loadedAsync: boolean = false;
  public userProfile: any;
  public listKeys: any = [];
  public screen = screen;
  public routers = {
    retrieveAll: "patient-list-recipe"
  };
  public currentPage = 1;
  public displayOption = {
    emptyList: true,
    hasList: false
  };

  constructor(private clientService: ClientService, public page: Page) {
    this.getUserInfo();
    // Remove o Action Bar padrão
    this.page.actionBarHidden = true;
  }

  ngAfterViewInit() {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAll(false);
    }, 450);
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Abre/fecha menu
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * Pega os itens.
   */
  public getAll(search) {
    this.clientService
      .getAllPaginate(
        this.routers.retrieveAll,
        this.userProfile.id,
        this.currentPage,
        ["error"],
        search
      )
      .then((response: any) => {
        this.loadedAsync = true;
        if (response.length > 0) {
          this.changeScreen("hasList");
          this.currentPage = this.currentPage + 1;
          response.forEach(item => {
            this.recipes.push(item);
          });
        }
      });
  }
  /**
   * Pagina mais itens.
   */
  public loadMore() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAll(false);
    }, 1000);
  }

  /**
   * Realiza a consulta do termo bucado.
   */
  public search(searchInfo) {
    this.loadedAsync = false;
    this.currentPage = 1;
    this.recipes = [];
    this.getAll(searchInfo.length > 0 ? searchInfo : false);
  }

  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }
}
