import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyRecipesComponent } from "./my-recipes.component";

const routes: Routes = [
    { path: "", component: MyRecipesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyRecipesRoutingModule { }
