import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyDoctorsDetailsComponent } from "./my-doctors-details.component";

const routes: Routes = [
    { path: "", component: MyDoctorsDetailsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyDoctorsDetailsRoutingModule { }
