import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyDoctorsDetailsRoutingModule } from "./my-doctors-details-routing.module";
import { MyDoctorsDetailsComponent } from "./my-doctors-details.component";
import { TNSFontIconModule } from "nativescript-ngx-fonticon";
/** Diretivas */
import { ContactUserModule } from "./../../../directives/contact-user/contact-user.module";
import { RecipeItemModule } from "./../../../directives/recipe-item/recipe-item.module";
import { ModalModule } from "./../../../directives/modal/modal.module";
@NgModule({
  imports: [
    ContactUserModule,
    RecipeItemModule,
    ModalModule,
    NativeScriptCommonModule,
    MyDoctorsDetailsRoutingModule,
    TNSFontIconModule.forRoot({
      mdi: "./fonts/icon-font-material-design-icons.css",
      fa: "./fonts/icon-font-awesome.css",
      icomoon: "./fonts/icon-font-icomoon.css"
    })
  ],
  declarations: [MyDoctorsDetailsComponent],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [MyDoctorsDetailsComponent]
})
export class MyDoctorsDetailsModule {}
