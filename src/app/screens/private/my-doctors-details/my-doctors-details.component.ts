import { Component, ViewChild } from "@angular/core";
import { ClientService, NotificationService } from "./../../../services";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import "moment/min/locales";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { ActivatedRoute } from "@angular/router";
import { screen } from "tns-core-modules/platform";
import { ModalComponent } from "./../../../directives/modal/modal.component";
import * as clipboard from "nativescript-clipboard";
@Component({
  selector: "MyDoctorsDetails",
  moduleId: module.id,
  templateUrl: "./my-doctors-details.component.html",
  styleUrls: ["./my-doctors-details.component.scss"]
})
export class MyDoctorsDetailsComponent {
  public delayTimer: any;
  public profile: any;
  public isAndroid = isAndroid;
  public isIOS = isIOS;
  public screen = screen;
  public searchQuery: string;
  public patients: any = null;
  public options: any;
  public now: any;
  public loadedAsync: boolean = false;
  public userProfile: any;
  public listKeys: any = [];
  public counter = 0;
  public menuList;
  public otherUserID: any;
  public otherUserInfo: any;
  public doctorAddress: any;
  public allRecipes: any = [];
  public routers = {
    getDoctorInfo: "patient-info-cad-doctor",
    retrieveAll: "patient-list-all-recipe-for-doctor"
  };
  public currentPage = 1;
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;
  constructor(
    private clientService: ClientService,
    private notificationService: NotificationService,
    public page: Page,
    private ActivatedRoute: ActivatedRoute
  ) {
    /** Remove o Action Bar padrão */
    this.page.actionBarHidden = true;
    /** Recebe os parâmetros da rota */
    this.ActivatedRoute.params.forEach(params => {
      this.otherUserID = params.other_user_id;
    });
    this.getUserInfo();
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getDoctorInfo();
      this.getAll();
    }, 450);
  }

  /**
   * Pega todos os dados do médico
   */
  public getDoctorInfo() {
    this.clientService
      .getById(this.routers.getDoctorInfo, this.otherUserID)
      .then((response: any) => {
        this.doctorAddress = {
          locality: response.address.locality,
          complete: `${response.address.street_title}, ${response.address.number}`,
          complement: response.address.complement,
          place: `${response.address.district_title}, ${response.address.city} - ${response.address.state}`,
          street_code: response.address.street_code
        };
        this.otherUserInfo = {
          name: response.personal.name,
          image: response.picture.image,
          telephone: response.contact.telephone_one,
          email: response.personal.email
        };
      });
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Pega todas as receitas do paciente
   */
  public getAll() {
    this.clientService
      .getAllPaginate(
        this.routers.retrieveAll,
        `${this.userProfile.id}/${this.otherUserID}`,
        this.currentPage,
        ["error"]
      )
      .then((response: any) => {
        this.loadedAsync = true;
        if (response.length > 0) {
          this.currentPage = this.currentPage + 1;
          this.allRecipes = response;
        }
      });
  }

  /**
   * Pagina mais itens.
   */
  public loadMore() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAll();
    }, 1000);
  }

  /**
   * Realiza a cópia do endereço.
   */
  public copyAddress() {
    const address = `${this.doctorAddress.complete} - ${this.doctorAddress.place}, ${this.doctorAddress.street_code}`;
    clipboard.setText(address).then(() => {
      this.notificationService.showFeedback({
        type: "success",
        message: "Endereço copiado com sucesso."
      });
    });
  }
}
