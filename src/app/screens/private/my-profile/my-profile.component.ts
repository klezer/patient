import { Component } from "@angular/core";
import {
  ClientService,
  NotificationService,
  GoogleAutoCompleteService,
  UtilsService
} from "./../../../services";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  ValueList,
  SelectedIndexChangedEventData
} from "nativescript-drop-down";
import { TextField } from "tns-core-modules/ui/text-field";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as moment from "moment";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Switch } from "tns-core-modules/ui/switch";
import * as utils from "tns-core-modules/utils/utils";
import * as frame from "tns-core-modules/ui/frame";

@Component({
  selector: "MyProfile",
  moduleId: module.id,
  templateUrl: "./my-profile.component.html",
  styleUrls: ["./my-profile.component.scss"]
})
export class MyProfileComponent {
  public delayTimer: any;
  public isAndroid = isAndroid;
  public isIOS = isIOS;
  public userProfile: any;
  public formMyProfile: any;
  public myProfile: any;
  public submitted: boolean = false;
  public inProccess: boolean = false;
  public genres: ValueList<string>;
  public FormGenres: any;
  public states: ValueList<string>;
  public FormStates: any;
  public citys: ValueList<string>;
  public FormCitys: any;
  public Address: any;
  public select_state_value: number = 0;
  public select_city_value: number = 0;
  public select_genre_value: number = 0;
  public loadedAsync: boolean = false;
  public loadedLists: boolean = false;
  public changePassword: boolean = false;
  public moment = moment;
  public rule = /[^a-z0-9]/gi;
  public routers = {
    updateAll: "patient-cad-update",
    retrieveAll: "patient-cad-get"
  };
  public updateImage = false;

  constructor(
    private googleAutoCompleteService: GoogleAutoCompleteService,
    private clientService: ClientService,
    private notificationService: NotificationService,
    public utilsService: UtilsService,
    public page: Page
  ) {
    this.page.actionBarHidden = true;
    this.getUserInfo();
    /** Cidades */
    this.FormCitys = [{ value: "", display: "Cidade" }];
    this.citys = new ValueList<string>(this.FormCitys);
    /** Estados */
    this.FormStates = [{ value: "", display: "UF" }];
    this.states = new ValueList<string>(this.FormStates);
    /** Gêneros */
    this.FormGenres = [
      { value: "", display: "Gênero" },
      { value: "F", display: "Feminino" },
      { value: "M", display: "Masculino" }
    ];
    this.genres = new ValueList<string>(this.FormGenres);
    /** Atualiza os estados */
    this.clientService.getInfo("/patient-cad-states").then(response => {
      this.FormStates = response;
      this.FormStates.splice(0, 0, { value: "", display: "UF" });
      this.states = new ValueList<string>(this.FormStates);
      /** Conclui a atualização das listas */
      this.loadedLists = true;
    });
    this.formControl(null);
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getMyProfileForEdit();
    }, 450);
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Pega todos os meus dados do perfil.
   */
  public getMyProfileForEdit() {
    this.clientService
      .getAll(this.routers.retrieveAll, this.userProfile.id, ["error"])
      .then((response: any) => {
        this.myProfile = response;
        this.updateLastImage();
        this.formControl(this.myProfile);
        this.resolveSelectField(
          this.myProfile.personal.genre,
          this.myProfile.address.state_id,
          this.myProfile.address.city_id
        );
      });
  }

  /**
   * Atualiza a última imagem
   */
  public updateLastImage() {
    if (this.userProfile.image !== this.myProfile.picture.image) {
      this.updateImage = true;
      this.userProfile.image = this.myProfile.picture.image;
      ApplicationSettings.setString(
        "userProfile",
        JSON.stringify(this.userProfile)
      );
    }
  }

  /**
   * Abre/fecha menu
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * Define todo os campos do formuário
   * com suas regras de validação.
   */
  private formControl(myProfile) {
    let form = {
      user_id: new FormControl(this.userProfile.id),
      name: new FormControl(myProfile != null ? myProfile.personal.name : "", [
        Validators.required
      ]),
      email: new FormControl(
        myProfile != null ? myProfile.personal.email : "",
        [Validators.required, Validators.email]
      ),
      telephone_one: new FormControl(
        myProfile != null ? myProfile.contact.telephone_one : "",
        [Validators.required]
      ),
      cpf: new FormControl(myProfile != null ? myProfile.personal.cpf : "", [
        Validators.required
      ]),
      rg: new FormControl(myProfile != null ? myProfile.personal.rg : "", [
        Validators.required
      ]),
      address_id: new FormControl(
        myProfile != null ? myProfile.address.address_id : "",
        [Validators.required]
      ),
      street_code: new FormControl(
        myProfile != null ? myProfile.address.street_code : "",
        [Validators.required]
      ),
      street_title: new FormControl(
        myProfile != null ? myProfile.address.street_title : "",
        [Validators.required]
      ),
      number: new FormControl(
        myProfile != null ? myProfile.address.number : "",
        [Validators.required]
      ),
      complement: new FormControl(
        myProfile != null ? myProfile.address.complement : ""
      ),
      district_title: new FormControl(
        myProfile != null ? myProfile.address.district_title : "",
        [Validators.required]
      ),
      genre: new FormControl("", [Validators.required]),
      document_professional: new FormControl("upload", [Validators.required]),
      date_of_birth: new FormControl(
        myProfile != null ? myProfile.personal.date_of_birth : "",
        [Validators.required]
      ),
      city_id: new FormControl("", [Validators.required]),
      state_id: new FormControl("", [Validators.required]),
      password: new FormControl(""),
      password_confirmation: new FormControl("")
    };
    // Se desejar renovar a senha
    if (this.changePassword) {
      form.password = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
      form.password_confirmation = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    this.formMyProfile = new FormGroup(form);
  }

  /**
   * Se desejar altera a senha.
   */
  public onChecked(args) {
    this.changePassword = (<Switch>args.object).checked;
    this.formControl(this.myProfile);
    this.resolveSelectField(
      this.myProfile.personal.genre,
      this.myProfile.address.state_id,
      this.myProfile.address.city_id
    );
  }

  /**
   * Define os valores corretos nos selects.
   */
  public resolveSelectField(genre, state, city) {
    // Definição de sexo
    let newGenre = /[^a-z]/gi.test(genre) ? (genre == 1 ? "F" : "M") : genre;
    this.select_genre_value = this.FormGenres.map(item => item.value).indexOf(
      newGenre.toUpperCase()
    );
    this.formMyProfile.controls.genre.setValue(this.select_genre_value);
    // Definição de estado
    this.select_state_value = this.FormStates.map(item => item.value).indexOf(
      state
    );
    this.formMyProfile.controls.state_id.setValue(this.select_state_value);
    // Definição de cidade
    this.autoCompleteCity(this.select_state_value).then(() => {
      this.select_city_value = parseInt(
        this.FormCitys.map(item => item.value).indexOf(city)
      );
      this.formMyProfile.controls.city_id.setValue(this.select_city_value);
    });
    this.loadedAsync = true;
  }

  /**
   * Consome a API do Google para preenchimento automático do endereco.
   */
  public getAddressByCep(args) {
    let input = <TextField>args.object;
    let onlyNumbers = input.text.replace(/[^0-9]/g, "");
    // Verifica se é númerico e possui a quantidade necessária para pesquisa
    if (/\d/.test(onlyNumbers) && onlyNumbers.length >= 7) {
      this.googleAutoCompleteService
        .getAddressByInfo(onlyNumbers, true)
        .then(response => {
          const address = this.googleAutoCompleteService.filterAddress(
            response
          );
          this.autoCompleteAddressFiels(
            address.street_title,
            address.district_title,
            address.mycity,
            address.state
          );
        });
    }
  }

  /**
   * Insere os dados nos campos de endeço.
   */
  public autoCompleteAddressFiels(street_title, district_title, mycity, state) {
    // define a rua
    if (street_title) {
      this.formMyProfile.controls["street_title"].setValue(street_title);
    }
    // define o bairro
    if (district_title) {
      this.formMyProfile.controls["district_title"].setValue(district_title);
    }
    // define o estado
    this.select_state_value = this.FormStates.map(item => item.display).indexOf(
      state
    );
    this.formMyProfile.controls["state_id"].setValue(
      this.FormStates[this.select_state_value].value
    );
    this.autoCompleteCity(this.select_state_value).then(() => {
      this.select_city_value = this.FormCitys.map(item => item.display).indexOf(
        mycity
      );
      this.formMyProfile.controls["city_id"].setValue(
        this.FormCitys[this.select_city_value].value
      );
    });
  }

  /**
   * Realiza o autocomplete do do estado/cidade.
   */
  public autoCompleteCity(state) {
    return new Promise(resolve => {
      this.clientService
        .getById("patient-cad-citys", this.FormStates[state].value)
        .then((response: any) => {
          this.FormCitys = response.citys;
          this.FormCitys.splice(0, 0, { value: "", display: "Selecione" });
          this.citys = new ValueList<string>(this.FormCitys);
          resolve(state);
        });
    });
  }

  /**
   * Observa as mudanças do componete de selecão e atribui os valores.
   */
  public changeSelect(args: SelectedIndexChangedEventData, field) {
    // Para estado
    if (field === "state_id") {
      this.autoCompleteCity(args.newIndex);
      this.formMyProfile.controls["state_id"].setValue(
        this.FormStates[args.newIndex].value
      );
      this.select_state_value = args.newIndex;
    }
    // Para gênero
    else if (field === "genre") {
      this.formMyProfile.controls[field].setValue(
        this.FormGenres[args.newIndex].value
      );
    }
    // Para cidade
    else if (field === "city_id") {
      this.formMyProfile.controls[field].setValue(
        this.FormCitys[args.newIndex].value
      );
      this.select_city_value = args.newIndex;
    }
  }

  /**
   * Define valores padrões para o campo de cidade.
   */
  public changeSelectClosed() {
    this.select_city_value = 0;
    this.formMyProfile.controls["city_id"].setValue();
  }

  /**
   * Realiza o cadastro do novo usuário.
   */
  public MyProfileUser() {
    this.submitted = true;
    let formatData = this.formatData(this.formMyProfile.value);
    if (this.changePassword) {
      if (
        this.formMyProfile.get("password").value !=
        this.formMyProfile.get("password_confirmation").value
      ) {
        this.notificationService.showFeedback({
          type: "warning",
          message: "As senhas não coincidem"
        });
        return false;
      }
    }
    if (formatData) {
      this.submitProfile(formatData);
    }
  }

  /**
   * Submete os dados já validados para a API.
   */
  public submitProfile(formatData) {
    this.utilsService.dismissSoftKeybaord();
    this.inProccess = true;
    this.clientService
      .postAll(this.routers.updateAll, formatData, true, ["error"])
      .then((response: any) => {
        // próximo passo do cadastro
        if (response.success) {
          this.notificationService.showFeedback({
            type: "success",
            message: response.success
          });
          this.inProccess = false;
        }
      });
  }

  /**
   * Formata os dados do formulário.
   */
  public formatData(value) {
    // Remove as máscaras
    let rule = /[^a-z0-9]/gi;
    let form = JSON.stringify(value);
    let newForm = JSON.parse(form);
    let formMyProfile = newForm;
    // Valida a data
    if (
      !this.formMyProfile.get("date_of_birth").invalid &&
      this.formMyProfile.get("date_of_birth") != null
    ) {
      let date = newForm["date_of_birth"].split("/");
      let year = date[2].replace(rule, "");
      let month = date[1].replace(rule, "");
      let day = date[0].replace(rule, "");
      if (year.length == 4 && (month.length && day.length) == 2) {
        formMyProfile.date_of_birth = year + "-" + month + "-" + day;
      } else {
        this.notificationService.showFeedback({
          type: "warning",
          message: "Data de nascimento inválido."
        });
        return false;
      }
    }
    // Valida outros campos
    formMyProfile.telephone_one =
      newForm["telephone_one"] != null
        ? newForm["telephone_one"].replace(rule, "")
        : "";
    formMyProfile.cpf =
      newForm["cpf"] != null ? newForm["cpf"].replace(rule, "") : "";
    formMyProfile.street_code =
      newForm["street_code"] != null
        ? newForm["street_code"].replace(rule, "")
        : "";

    if (
      newForm["telephone_one"].length < 10 ||
      newForm["cpf"].length < 11 ||
      newForm["street_code"].length < 8
    ) {
      this.notificationService.showFeedback({
        type: "warning",
        message: "Verifique os campos inválidos."
      });
      return false;
    }

    // Retorna os valores tratados
    return formMyProfile;
  }
}
