import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { MessagesDetailsRoutingModule } from "./messages-details-routing.module";
import { MessagesDetailsComponent } from "./messages-details.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ReactiveFormsModule } from '@angular/forms'; 
/** Diretivas */
import { ContactUserModule } from './../../../directives/contact-user/contact-user.module';
import { MessageItemModule } from './../../../directives/message-item/message-item.module';

@NgModule({
    imports: [
        ContactUserModule,
        MessageItemModule,
        NativeScriptCommonModule,
        MessagesDetailsRoutingModule,
        NativeScriptFormsModule,
        ReactiveFormsModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        MessagesDetailsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MessagesDetailsComponent
    ]
})
export class MessagesDetailsModule { }
