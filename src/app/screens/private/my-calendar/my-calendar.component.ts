import { Component } from "@angular/core";
import { ClientService } from "./../../../services";
import { isAndroid } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as calendarModule from "nativescript-ui-calendar";
import { Color } from "tns-core-modules/color";
@Component({
  selector: "MyCalendar",
  moduleId: module.id,
  templateUrl: "./my-calendar.component.html",
  styleUrls: ["./my-calendar.component.scss"]
})
export class MyCalendarComponent {
  public delayTimer: any;
  public isAndroid = isAndroid;
  public screen: any;
  public loadedAsync: boolean = false;
  public userProfile: any;
  public listKeys: any = false;
  public routers = {
    retrieveAll: "patient-treatment-calendar"
  };
  public searchInfo: string;
  public emptyList: boolean = true;
  public currentPage: number = 0;

  public calendarEvents = [];
  public monthViewStyle: any;
  public dayCellStyle: any;
  public titleCellStyle: any;
  public dayNameCellStyle: any;
  public todayCellStyle: any;
  public inlineEventCellStyle: any;
  public selectedCellStyle: any;

  constructor(private clientService: ClientService, public page: Page) {
    this.page.actionBarHidden = true;
    this.getUserInfo();
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * Abre/fecha menu.
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getAllEvents();
      this.calendarCustomized();
    }, 450);
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Busca os eventos.
   * */
  public getAllEvents() {
    this.clientService
      .getAll(this.routers.retrieveAll, this.userProfile.id, ["error"])
      .then((response: any) => {
        response.forEach((item, index) => {
          let date = new Date(item.start_period * 1000);
          this.createEvent(item, date);
          if (response.length - 1 == index) this.loadedAsync = true;
        });
      });
  }

  /**
   * Exibe os eventos do calendário.
   */
  public createEvent(item, date) {
    let finalDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes()
    );
    let color = isAndroid ? "#FFFFFF" : "#4FB7DB";
    let event = new calendarModule.CalendarEvent(
      item.title,
      finalDate,
      finalDate,
      false,
      new Color(color)
    );
    this.calendarEvents.push(event);
  }

  /**
   * Customização do calendário.
   */
  public calendarCustomized() {
    // Definições do cabeçalho
    this.monthViewStyle = new calendarModule.CalendarMonthViewStyle();
    this.monthViewStyle.backgroundColor = "#FDFBFB";
    this.monthViewStyle.showTitle = true;
    this.monthViewStyle.showWeekNumbers = false;
    this.monthViewStyle.showDayNames = true;
    this.monthViewStyle.cellAlignment = "Left";
    // Definições do dia selecionado
    this.monthViewStyle.selectionShape = "Round";
    this.monthViewStyle.selectionShapeSize = 20;
    this.monthViewStyle.selectionShapeColor = "#4FB7DB";
    this.selectedCellStyle = new calendarModule.DayCellStyle();
    this.selectedCellStyle.eventFontStyle = "Bold";
    this.selectedCellStyle.eventTextSize = 8;

    this.selectedCellStyle.cellAlignment = "Center";
    this.selectedCellStyle.cellPaddingHorizontal = 5;
    this.selectedCellStyle.cellPaddingVertical = 5;
    this.selectedCellStyle.cellBackgroundColor = "#046B9E";
    this.selectedCellStyle.cellTextColor = "White";
    this.monthViewStyle.selectedDayCellStyle = this.selectedCellStyle;
    // Definições para o dia de hoje
    this.todayCellStyle = new calendarModule.DayCellStyle();
    this.todayCellStyle.cellBackgroundColor = "#F0F1F4";
    this.todayCellStyle.cellBorderWidth = 1;
    this.todayCellStyle.cellBorderColor = "#4FB7DB";
    this.todayCellStyle.cellTextColor = "#4FB7DB";
    this.todayCellStyle.cellTextSize = 14;
    this.monthViewStyle.todayCellStyle = this.todayCellStyle;
    // Definições da lista de evento do selecionado
    this.inlineEventCellStyle = new calendarModule.InlineEventCellStyle();
    this.inlineEventCellStyle.timeTextColor = "#FFFFFF";
    this.inlineEventCellStyle.cellBackgroundColor = "#046B9E";
    this.inlineEventCellStyle.cellBorderWidth = 1;
    this.inlineEventCellStyle.cellBorderColor = "#4FB7DB";
    this.inlineEventCellStyle.eventTextColor = "#FFFFFF";
    this.monthViewStyle.inlineEventCellStyle = this.inlineEventCellStyle;
    // Definições do título do calendário
    this.titleCellStyle = new calendarModule.DayCellStyle();
    this.titleCellStyle.cellBackgroundColor = "#FDFBFB";
    this.titleCellStyle.cellBorderWidth = 1;
    this.titleCellStyle.cellBorderColor = "#FDFBFB";
    this.titleCellStyle.cellTextColor = "#666666";
    this.titleCellStyle.cellTextFontStyle = "Bold";
    this.titleCellStyle.cellTextSize = 18;
    this.monthViewStyle.titleCellStyle = this.titleCellStyle;
    // Definições dos dias do calendário
    this.dayNameCellStyle = new calendarModule.CellStyle();
    this.dayNameCellStyle.cellBackgroundColor = "#FDFBFB";
    this.dayNameCellStyle.cellBorderWidth = 1;
    this.dayNameCellStyle.cellBorderColor = "#FDFBFB";
    this.dayNameCellStyle.cellTextColor = "#B9B9B9";
    this.dayNameCellStyle.cellTextFontStyle = "Bold";
    this.dayNameCellStyle.cellTextSize = 10;
    this.monthViewStyle.dayNameCellStyle = this.dayNameCellStyle;
  }

  // Eventos do calendário
  public onDateSelected(args) {
    console.log("onDateSelected: " + args.date);
  }

  public onDateDeselected(args) {
    console.log("onDateDeselected: " + args.date);
  }

  public onNavigatedToDate(args) {
    console.log("onNavigatedToDate: " + args.date);
  }

  public onNavigatingToDateStarted(args) {
    console.log("onNavigatingToDateStarted: " + args.date);
  }

  public onViewModeChanged(args) {
    console.log("onViewModeChanged: " + args.newValue);
  }
}
