import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyCalendarRoutingModule } from "./my-calendar-routing.module";
import { MyCalendarComponent } from "./my-calendar.component";

import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { SearchTopBarModule } from './../../../directives/search-top-bar/search-top-bar.module';

@NgModule({
    imports: [
        SearchTopBarModule,
        NativeScriptCommonModule,
        MyCalendarRoutingModule,
        NativeScriptUICalendarModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        MyCalendarComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MyCalendarComponent
    ]
})
export class MyCalendarModule { }
