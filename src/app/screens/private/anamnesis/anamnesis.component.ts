import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ClientService, NotificationService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Switch } from "tns-core-modules/ui/switch";
import { TextField } from "tns-core-modules/ui/text-field";

@Component({
  selector: "anamnesis",
  moduleId: module.id,
  templateUrl: "./anamnesis.component.html",
  styleUrls: ["./anamnesis.component.scss"]
})
export class AnamnesisComponent {
  public delayTimer: any;
  public userProfile: any;
  public loadedAsync: boolean = false;
  public patientID: any;
  public routers = {
    getStructure: "patient-anamnese-structure",
    getMyAnamnese: "patient-anamnese-get",
    createAnamnese: "patient-anamnese-create",
    updateAnamnese: "patient-anamnese-update"
  };

  public anamneseInfo: any = [];
  public myAnamneseInfo: any = [];
  public isRetrieve = false;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private clientService: ClientService,
    private notificationService: NotificationService,
    public page: Page
  ) {
    this.page.actionBarHidden = true;
    this.getUserInfo();
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * Abre/fecha menu.
   */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getStructure();
    }, 450);
  }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
  public getUserInfo() {
    if (ApplicationSettings.hasKey("userProfile")) {
      this.userProfile = JSON.parse(
        ApplicationSettings.getString("userProfile")
      );
    }
  }

  /**
   * Realiza a montagem da estrutura de perguntas.
   */
  public getStructure() {
    this.clientService
      .getAll(this.routers.getStructure, null, ["error"])
      .then((response: any) => {
        this.anamneseInfo = response;
        this.loadedAsync = true;
        this.insertMyInfoInStructure();
      });
  }

  /**
   * Atualiza os dados já existente do usuário na estrutura padrão.
   */
  public insertMyInfoInStructure() {
    this.clientService
      .getById(this.routers.getMyAnamnese, this.userProfile.id)
      .then((response: any) => {
        if (response.length > 0) {
          this.myAnamneseInfo = response;
          this.isRetrieve = true;
          this.populateFields(this.myAnamneseInfo);
        }
      });
  }

  /**
   * Popula os campos das estrtura.
   */
  public populateFields(list) {
    list.forEach(resp => {
      if (resp.value == 1) {
        let index = this.anamneseInfo
          .map(structure => structure.topic_id)
          .indexOf(resp.topic_id);
        this.anamneseInfo[index].value = true;
        if (this.anamneseInfo[index].sub_title_quetion !== undefined) {
          resp.sub_title_quetion.forEach((element, i) => {
            this.anamneseInfo[index].sub_title_quetion[i].value =
              element.answer;
          });
        }
        console.dir(this.anamneseInfo[index]);
      }
    });
  }

  /**
   * Ao selecionar o tipo de anaminese.
   */
  public onChecked(args, item) {
    let choice = (<Switch>args.object).checked;
    if (choice) {
      item.value = true;
      if (item.sub_title_quetion != undefined) {
        item.sub_title_quetion.value =
          item.sub_title_quetion.value != undefined
            ? item.sub_title_quetion.value
            : "";
      }
    } else {
      item.value = false;
    }
  }

  /**
   * Ao alterar a descrição da anaminese e
   * responde negativamente se a descrição for removida.
   */
  public changeInfo(args, item) {
    let text = (<TextField>args.object).text;
    item.value = text;
  }

  /**
   * Salva os dados informados pelo paciente.
   */
  public saveAnaminesis() {
    let valid = true;
    let listErrors = [];
    this.anamneseInfo.forEach(topic => {
      topic.value = topic.value !== undefined ? topic.value : false;
      //Valida se houver subtópico vinculado ao tópico.
      if (topic.sub_title_quetion !== undefined && topic.value) {
        topic.sub_title_quetion.forEach(subtopic => {
          subtopic.invalid = false;
          subtopic.value = subtopic.value !== undefined ? subtopic.value : "";
          // Valida se o subtópico foi respondido
          if (subtopic.value.length == 0) {
            valid = false;
            subtopic.invalid = true;
            listErrors.push(subtopic.title);
          }
        });
      }
      // Remove os resquícios da mudança de escolha
      if (
        topic.value !== undefined &&
        topic.sub_title_quetion !== undefined &&
        !topic.value
      ) {
        topic.sub_title_quetion.forEach(subtopic => {
          if (subtopic.value !== undefined) {
            delete subtopic.value;
            delete subtopic.invalid;
          }
        });
      }
    });
    // Valida as informações
    if (valid) {
      this.submit();
    } else {
      this.notificationService.showFeedback({
        type: "warning",
        message: `${
          listErrors.length == 1
            ? "Informe " + listErrors[0].toLowerCase().replace("?", ".")
            : "Responda as perguntas em aberto."
        }`
      });
    }
  }

  /**
   * Submete os dados informados pelo paciente.
   */
  public submit() {
    const answer = {
      user_id: this.userProfile.id,
      answer: this.anamneseInfo
    };
    let finalRoute = !this.isRetrieve
      ? this.routers.createAnamnese
      : this.routers.updateAnamnese;
    this.clientService.postAll(finalRoute, answer, false, ["error", "success"]);
  }
}
