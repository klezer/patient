import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { RegisterImageRoutingModule } from "./register-image-routing.module";
import { RegisterImageComponent } from "./register-image.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        RegisterImageRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
        }),
        HttpModule
    ],
    declarations: [
        RegisterImageComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RegisterImageModule { }
