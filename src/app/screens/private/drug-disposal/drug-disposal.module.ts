import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DrugDisposalRoutingModule } from "./drug-disposal-routing.module";
import { DrugDisposalComponent } from "./drug-disposal.component";
import { TNSFontIconModule } from "nativescript-ngx-fonticon";
/** Diretivas */
import { SearchTopBarModule } from "./../../../directives/search-top-bar/search-top-bar.module";
import { ModalModule } from "./../../../directives/modal/modal.module";
@NgModule({
  imports: [
    SearchTopBarModule,
    ModalModule,
    NativeScriptCommonModule,
    DrugDisposalRoutingModule,
    TNSFontIconModule.forRoot({
      mdi: "./fonts/icon-font-material-design-icons.css",
      fa: "./fonts/icon-font-awesome.css",
      icomoon: "./fonts/icon-font-icomoon.css"
    })
  ],
  declarations: [DrugDisposalComponent],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [DrugDisposalComponent]
})
export class DrugDisposalModule {}
