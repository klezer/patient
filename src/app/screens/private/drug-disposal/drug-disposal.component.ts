import { Component, ViewChild } from "@angular/core";
import { ClientService, NotificationService } from "./../../../services";
import { isAndroid } from "tns-core-modules/platform";
import "moment/min/locales";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Page } from "tns-core-modules/ui/page";
import {
  MapView,
  Marker,
  Position,
  Camera
} from "nativescript-google-maps-sdk";
import * as geolocation from "nativescript-geolocation";
import { Image } from "tns-core-modules/ui/image/image";
import { ImageSource, fromFile } from "tns-core-modules/image-source";
import { registerElement } from "nativescript-angular/element-registry";
import * as utils from "tns-core-modules/utils/utils";
import { ModalComponent } from "./../../../directives/modal/modal.component";
registerElement(
  "MapView",
  () => require("nativescript-google-maps-sdk").MapView
);

@Component({
  selector: "DrugDisposal",
  moduleId: module.id,
  templateUrl: "./drug-disposal.component.html",
  styleUrls: ["./drug-disposal.component.scss"]
})
export class DrugDisposalComponent {
  public delayTimer: any;
  public loadedAsync: boolean = false;
  public isAndroid = isAndroid;
  public minZoom = 0;
  public maxZoom = 22;
  public bearing = 0;
  public tilt = 0;
  public mapView: MapView & { infoWindowTemplates: string };
  public myCamera: Camera;
  public gMap: any;
  public currentPosition: any = {
    latitude: -23.5505,
    longitude: -46.6333,
    title: "São Paulo",
    snippet: "Brasil",
    zoom: 13
  };
  public lastCamera: String;
  public watchId: number;
  public canMonitor: boolean = false;
  public myImagePin: string = `~/image-source/Pin-${
    isAndroid ? "android" : "ios"
  }.png`;
  public AllImageForPinsEnabled: string = `~/image-source/Pin-actived-${
    isAndroid ? "android" : "ios"
  }.png`;
  public routers = {
    getPoints: "discart-points-list"
  };
  public listPoints: any = [];
  public allMarkers: any = [];
  public currentPin = null;
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;
  constructor(
    private clientService: ClientService,
    private notificationService: NotificationService,
    public page: Page
  ) {
    // Remove o Action Bar padrão
    this.page.actionBarHidden = true;
  }

  ngOnInit(): void {
    this.onDrawerButtonTap(false);
  }

  ngAfterContentInit(): void {
    this.loadedConfirm();
  }

  /**
   * Busca todos os registros
   * */
  public getAllPoints(router, obj) {
    this.clientService.getAll(router, obj, ["false"]).then((response: any) => {
      this.listPoints = response;
      this.insertAllPlaces();
    });
  }

  /**
   * confirmação de carregamento da API.
   */
  public loadedConfirm() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.isEnabled();
      this.getAllPoints(this.routers.getPoints, null);
    }, 450);
  }

  /**
   * Abre/fecha menu
   * */
  public onDrawerButtonTap(confirm): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (sideDrawer !== undefined && sideDrawer.getIsOpen()) {
      sideDrawer.closeDrawer();
    }
    if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm) {
      sideDrawer.showDrawer();
    }
  }

  /**
   * Recupera o evento do carregamento do Mapa.
   */
  public onMapReady(event) {
    this.mapView = event.object;
    this.gMap = this.mapView.gMap;
    // Definiçõs para Android
    if (isAndroid) {
      let uiSettings = this.gMap.getUiSettings();
      uiSettings.setMyLocationButtonEnabled(false);
      this.gMap.setMyLocationEnabled(true);
    }
    // Definiçõs para iOS
    if (!isAndroid) {
      this.gMap.myLocationEnabled = true;
      this.gMap.settings.myLocationButton = false;
    }
  }

  /**
   * Verifica se a geolocalização está permitida.
   * isEnabled, enableLocationRequest, getCurrentLocation, watchLocation, distance, clearWatch
   */
  public isEnabled() {
    geolocation.isEnabled().then(isEnabled => {
      if (!isEnabled) {
        this.enableLocationRequest();
      } else {
        this.insertAllPlaces();
      }
    });
  }

  /**
   * Solicita o acesso as coordenadas do GPS.
   */
  public enableLocationRequest() {
    geolocation.enableLocationRequest().then(() => {
      this.insertAllPlaces();
    });
  }

  /**
   * Insere os pontos de descarte no Mapa
   */
  public insertAllPlaces() {
    this.listPoints.forEach(local => {
      this.insertMarkInMap(local, this.AllImageForPinsEnabled);
    });
  }

  /**
   * Encontra minha localização atual.
   */
  public getInitialLocation(zoom, timeout) {
    geolocation
      .getCurrentLocation({
        desiredAccuracy: 3,
        updateDistance: 10,
        maximumAge: 20000,
        timeout: timeout
      })
      .then((loc: any) => {
        if (loc) {
          this.currentPosition.latitude = loc.latitude;
          this.currentPosition.longitude = loc.longitude;
          this.currentPosition.zoom = zoom;
          this.startsPositionMonitoring();
        }
        this.loadedAsync = true;
      });
  }

  /**
   * Insere e posiciona os marcadores de posição no Mapa.
   */
  public insertMarkInMap(coordinates, pin) {
    var marker = new Marker();
    let maskFone = coordinates.fone;
    // Define os dados do template no marcador
    marker.userData = {
      new_title: coordinates.title,
      street_title: coordinates.street_title,
      district: coordinates.district,
      city: coordinates.city,
      fone:
        "(" +
        maskFone.substr(0, 2) +
        ") " +
        maskFone.substr(3, 4) +
        "-" +
        maskFone.substr(7, maskFone.length),
      index: 1,
      coordinates: {
        latitude: coordinates.latitude,
        longitude: coordinates.longitude
      }
    };
    // Define as informações do marcador
    marker.position = Position.positionFromLatLng(
      coordinates.latitude,
      coordinates.longitude
    );
    marker.icon = this.pinCustomized(pin);
    this.mapView.addMarker(marker);
    this.allMarkers.push(marker);
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getInitialLocation(13, 0);
    }, 1000);
  }

  /**
   * Customiza os marcadores de acordo com o arquivo informado.
   */
  public pinCustomized(pin) {
    const markerImage: ImageSource = fromFile(pin);
    const icon = new Image();
    icon.src = markerImage;
    icon.imageSource = markerImage;
    icon.height = 10;
    icon.stretch = "aspectFit";
    return icon;
  }

  /**
   * Monitora os eventos dos Pins no Mapa.
   */
  public onMarkerEvent(args) {
    if (args.eventName === "markerSelect") {
      this.currentPin = args.marker.userData;
      this.modal.show();
    }
  }

  /**
   * showInGMaps
   */
  public showInGMaps(pin) {
    let address = isAndroid
      ? `https://maps.google.com/maps?daddr=${pin.coordinates.latitude},${
          pin.coordinates.longitude
        }&amp;ll=`
      : `maps://maps.google.com/maps?daddr=${pin.coordinates.latitude},${
          pin.coordinates.longitude
        }&amp;ll=`;
    utils.openUrl(address);
  }

  /**
   * closeModal
   */
  public closeModal() {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.modal.hide();
      this.currentPin = null;
    }, 300);
  }

  /**
   * Realiza a pesquisa dos pontos no Mapa.
   */
  public search(searchInfo) {
    let result = this.allMarkers.filter(item => {
      return (
        item.userData.new_title
          .toUpperCase()
          .includes(searchInfo.toUpperCase()) ||
        item.userData.fone.toUpperCase().includes(searchInfo.toUpperCase()) ||
        item.userData.street_title
          .toUpperCase()
          .includes(searchInfo.toUpperCase()) ||
        item.userData.city.toUpperCase().includes(searchInfo.toUpperCase()) ||
        item.userData.district.toUpperCase().includes(searchInfo.toUpperCase())
      );
    });
    // Se houver resultado
    if (result.length) {
      this.allMarkers.forEach(item => {
        item.visible = false;
        item.alpha = 0;
        item.zIndex = 0;
        result.forEach(child => {
          if (item == child) {
            item.visible = true;
            item.alpha = 1;
            item.zIndex = 1;
          }
        });
      });
      // Move a camera
      if (result.length == 1) {
        this.currentPosition.latitude = result[0].position.latitude;
        this.currentPosition.longitude = result[0].position.longitude;
        this.currentPosition.zoom = 16;
      } else {
        this.currentPosition.zoom = searchInfo.length == 0 ? 16 : 7;
      }
    } else {
      this.notificationService.showAlert({
        type: "showInfo",
        title: "Oops.",
        message: "Nenhum ponto de descarte encontrado!",
        button: "Acontece né!"
      });
    }
    // Se o campo for limpo
    if (searchInfo.length == 0) {
      this.allMarkers.forEach(item => {
        item.visible = true;
        item.alpha = 1;
        item.zIndex = 1;
      });
    }
  }

  /**
   * Inicia o monitoramento de deslocamento do aparelho.
   */
  public startsPositionMonitoring() {
    this.watchId = geolocation.watchLocation(
      loc => {
        if (loc) {
          this.currentPosition = loc;
          this.currentPosition.zoom = 11;
        }
      },
      function(e) {
        console.log("Error: startsPositionMonitoring" + e.message);
      },
      { desiredAccuracy: 3, updateDistance: 10, minimumUpdateTime: 1000 * 5 }
    ); // Should update every 20 seconds according to Googe documentation. Not verified.
  }

  /**
   * Interrompe o rastreamento de deslocamento do aparelho.
   */
  public stopPositionMonitoring() {
    geolocation.clearWatch(this.watchId);
  }
}
