import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { DrugDisposalComponent } from "./drug-disposal.component";

const routes: Routes = [
    { path: "", component: DrugDisposalComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DrugDisposalRoutingModule { }
