import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import {
  ClientService,
  FirebaseService,
  NotificationService,
  UtilsService
} from "./../../../services";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "Home",
  moduleId: module.id,
  styleUrls: ["./home.component.scss"],
  templateUrl: "./home.component.html"
})
export class HomeComponent {
  public form: any;
  public isAndroid = isAndroid;
  public isIOS = isIOS;
  public submitted: boolean = false;
  public inProccess: boolean = false;
  public not_show_password: boolean = true;
  public kepp_login: boolean;
  public loadedAsync: boolean = false;
  public displayOption = {
    login: true,
    forgetLogin: false,
    confirmAccountPassword: false,
    newPassword: false
  };
  public routers = {
    login: "api-login",
    forgotPassword: "send-email-reset",
    validateSms: "validate-code-reset",
    resetPassword: "password-reset"
  };
  constructor(
    public page: Page,
    private clientService: ClientService,
    private firebaseService: FirebaseService,
    private notificationService: NotificationService,
    private utilsService: UtilsService
  ) {
    this.page.actionBarHidden = true;
    if (ApplicationSettings.hasKey("registrationSteps")) {
      this.utilsService.goToScreen("/register-image");
    } else {
      this.clientService.isLogged().then(() => {
        this.utilsService.goToScreen("/dashboard");
      });
    }
  }

  ngAfterContentInit(): void {
    this.formControl("login");
    this.loadedAsync = true;
  }

  /**
   * Define todo os campos do formuário
   * com suas regras de validação.
   */
  private formControl(type) {
    let form = {
      email: new FormControl(""),
      password: new FormControl(""),
      code: new FormControl("")
    };

    if (type == "login") {
      form.email = new FormControl("", [Validators.required, Validators.email]);
      form.password = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    if (type == "code") {
      form.code = new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]);
    }
    if (type == "reset") {
      form.password = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    this.form = new FormGroup(form);
  }

  /**
   * Realiza o login do usuário.
   */
  public submitLogin() {
    this.utilsService.dismissSoftKeybaord();
    this.submitted = true;
    this.firebaseService.getCurrentPushToken().then((response: any) => {
      ApplicationSettings.setString("deviceToken", response);
      // Valida o formuário
      if (this.form.valid) {
        this.inProccess = true;
        let form = this.form.value;
        form.device_token = ApplicationSettings.getString("deviceToken");
        this.clientService
          .postAll(this.routers.login, form, true, ["error"])
          .then((response: any) => {
            this.submitted = false;
            if (response.success) {
              this.clientService.setApiServiceTokenApi(response.success.token);
              ApplicationSettings.setString(
                "userProfile",
                JSON.stringify(response.success)
              );
              this.clientService.resolveResponse("/dashboard");
            }
          });
      } else {
        this.submitted = false;
      }
    });
  }

  /**
   * Inicia a recuperação da senha do usuário.
   */
  public recoveryPassword() {
    const form = {
      email: this.form.get("email").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.forgotPassword, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          ApplicationSettings.setNumber("recoveryID", response.success.user_id),
            this.changeScreen("confirmAccountPassword");
          this.formControl("code");
        }
      });
  }

  /**
   * Realiza a validação do código de recuperação de senha.
   */
  public validateCode() {
    const form = {
      user_id: ApplicationSettings.getNumber("recoveryID"),
      code: this.form.get("code").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.validateSms, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          this.changeScreen("newPassword");
          this.formControl("reset");
        }
      });
  }

  /**
   * Define um nova senha.
   */
  public redefinePassword() {
    const form = {
      user_id: ApplicationSettings.getNumber("recoveryID"),
      password: this.form.get("password").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.resetPassword, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          this.notificationService.showFeedback({
            type: "success",
            message: "Senha alterada com sucesso!"
          });
          this.changeScreen("login");
        }
      });
  }

  /**
   * forgetPassword
   */
  public forgetPassword() {
    dialogs
      .prompt({
        title: "RECUPERAÇÃO DE SENHA",
        message: "Informe o seu e-mail para iniciar o processo.",
        okButtonText: "Recuperar",
        cancelButtonText: "Cancelar",
        inputType: dialogs.inputType.email
      })
      .then(r => {
        console.log("Dialog result: " + r.result + ", text: " + r.text);
      })
      .catch(error => console.log(error));
  }

  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }
}
